--[[
    privlockdown- Harcoded Player Privileges Whitelist
    Copyright (C) 2022  R1BNC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]--


-- Hardcoded player whitelist
local whitelisted_players = {
    "r1bnc",
    "dew"
}

-- Check every player who joins
minetest.register_on_joinplayer(function(player)
    local playername = player:get_player_name()
    local currentplayerprivs = minetest.get_player_privs(playername)
    local matched = false

    -- Loop Whitelist
    for _, whitelisted_player in pairs(whitelisted_players) do
        -- Whitelisted player matched
        if playername == whitelisted_player then
            minetest.chat_send_player(playername, minetest.colorize("#3cb371", "[Server] Hello whitelisted player"))
            matched = true
            break
        end
    end
    -- Not whitelisted player
    if not matched then
        minetest.log("action", "[privlockdown] Reset privs for: ".. playername .. " - Old privs: " .. dump(currentplayerprivs,''))
        -- Change based on your basic_priv config
        minetest.set_player_privs(playername, {interact = true, shout = true, home = true, tp = true })
    end

end)