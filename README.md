# privlockdown_r1

A simple server side mod to prevent unauthorized granting of privs to other players unless in whitelist.
Every player who joins will get their privs reset back to the defaults.

## How to use?

Edit init.lua to specify whitelisted names and privs.

## Dependencies

None

## Version

1.0

## Roadmap
1. Logging
2. Settings

## License
```
    privlockdown - Harcoded Player Privileges Whitelist
    Copyright (C) 2022  R1BNC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
